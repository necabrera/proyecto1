package parqueadero;
//clase tomada del taller 3
public class Pila<T> 
{
	private class Node<T>
	{
		private T item;

		private Node<T> next;

		public Node(T pItem, Node pNext)
		{
			next = pNext;
			item = pItem;
		}

		public T getItem()
		{
			return item;
		}

		public Node getNext()
		{
			return next;
		}
		
		public void setItem( T pItem)
		{
			item = pItem; 
		}
		
		public void setNext (Node nodo)
		{
			next = nodo;
		}

	}
	private Node<T> top;

	private int size;

	

	public Pila()
	{
		this.top = null;
		size = 0;
	}
	
	public boolean vacio()
	{
		return top == null;
	}
	
	public int getSize()
	{
		return size;
	}
	
	public T darItemTop()
	{
		if(vacio())
		{
			return null;
		}
		return top.getItem();
	}
	
	public Node darNodoTop()
	{
		return top;
	}
	
	
	public void push(T item)
	{
		top = new Node<T>(item, top);
		size ++;
	}
	
	public T pop()
	{
		T objeto = null;
		if(top != null)
		{
			objeto = top.getItem();
			top = top.getNext();
			size--;
		}
		return objeto;
	}

}
