package parqueadero;
//clase tomada del taller 3
public class Cola <T>
{
	private Node<T> primero;

	private int size;

	private class Node<T>
	{
		private T item;

		private Node<T> next;

		public Node()
		{
			next = null;
		}

		public T getItem()
		{
			return item;
		}

		public Node getNext()
		{
			return next;
		}

		public void setItem( T pItem)
		{
			item = pItem; 
		}

		public void setNext (Node nodo)
		{
			next = nodo;
		}

	}

	public Cola()
	{
		primero = null;
		size = 0;
	}

	public int getSize()
	{
		return size;
	}

	public boolean vacio()
	{
		if(primero == null)
			return true;
		else return false;

	}

	public T dequeue()
	{
		T item = null;
		if(!vacio())
		{
			item = primero.getItem();
			primero = primero.getNext();
			size --;
		}
		return item;
	}
	
	public T getItem()
	{
		return primero.getItem();
	}

	public void enqueue(T item)
	{
		Node<T> actual = primero;
		Node<T> nuevo = new Node<T>();
		nuevo.setItem(item);

		if(primero == null)
		{
			primero = nuevo;
			size ++;
			return;
		}
		while(actual.getNext() != null)
		{
			actual = actual.getNext();
		}
		actual.setNext(nuevo);
		size ++;


	}


}
