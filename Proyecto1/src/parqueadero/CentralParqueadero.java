package parqueadero;
//clase tomada del taller 3

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

import com.csvreader.CsvReader;

import mundo.Lista;

public class CentralParqueadero 
{

	/**
	 * Cola de carros en espera para ser estacionados
	 */
	private Cola<Carro> cola;


	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 5
	 */


	private Pila<Carro> pila1;
	private Pila<Carro> pila2;
	private Pila<Carro> pila3;
	private Pila<Carro> pila4;
	private Pila<Carro> pila5;

	private Carro cliente;

	/**
	 * Pila de carros parqueadero temporal:
	 * Aca se estacionan los carros temporalmente cuando se quiere
	 * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
	 */
	private Pila<Carro> aux;


	/**
	 * Inicializa el parqueadero: Los parqueaderos (1,2... 5) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
	 */
	public CentralParqueadero()
	{
		cola =   new Cola();
		pila1 =  new Pila();
		pila2 =  new Pila();
		pila3 =  new Pila();
		pila4 =  new Pila();
		pila5 =  new Pila();
		aux =    new Pila();
		cliente = null;

	}
	public  void inicializarCarros() throws IOException
	{ 
		String nombre = "";
		String matricula = "";
		String color = "";
		String separador = ";";
		BufferedReader br = null;
		try {
			br =new BufferedReader(new FileReader("./data/Ingreso_al_parqueadero.csv"));
			br.readLine();
			String line = br.readLine();
			while (line != null) 
			{
				String [] fields = line.split(separador);
				nombre = fields[1];
				matricula =fields[2] ;
				color = fields[3];
				line = br.readLine();
				cola.enqueue(registrarCliente(color, matricula, nombre));
			}
		} catch (Exception e) {

		} finally {
			if (null!=br) {
				br.close();
			}
		}

	}

	/**
	 * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
	 * @param pColor color del vehiculo
	 * @param pMatricula matricula del vehiculo
	 * @param pNombreConductor nombre de quien conduce el vehiculo
	 */
	public Carro registrarCliente (String pColor, String pMatricula, String pNombreConductor)
	{
		cliente = new Carro(pColor, pMatricula, pNombreConductor);
		return cliente;
	}       



	/**
	 * Saca del parqueadero los vehiculos de los alcaldes
	 * @return cantidad de pasos en los que se vacio el parqueadero
	 * @throws Exception si no encuentra el carro
	 */
	public int sacarCarros() throws Exception
	{int contador = 0;

	pila1.pop();
	contador++;
	pila1.pop();
	contador++;
	pila1.pop();
	contador++;
	pila1.pop();
	contador++;
	pila1.pop();
	contador++;
	pila2.pop();
	contador++;
	pila2.pop();
	contador++;
	pila2.pop();
	contador++;
	pila2.pop();
	contador++;
	pila2.pop();
	contador++;
	pila3.pop();
	contador++;
	pila3.pop();
	contador++;
	pila3.pop();
	contador++;
	pila3.pop();
	contador++;
	pila3.pop();
	contador++;
	pila4.pop();
	contador++;
	pila4.pop();
	contador++;
	pila4.pop();
	contador++;
	pila4.pop();
	contador++;
	pila4.pop();
	contador++;
	pila5.pop();
	contador++;
	return contador;
	}

	/**
	 * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
	 * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
	 * @return El parqueadero en el que qued� el carro
	 * @throws Exception
	 */
	public int parquearCarros() throws Exception
	{int contador = 0;
	aux.push(cola.dequeue());
	contador++;
	pila4.push(cola.dequeue());
	contador++;
	pila3.push(cola.dequeue());
	contador++;
	pila3.push(cola.dequeue());
	contador++;
	pila3.push(aux.pop());
	contador++;
	pila4.push(cola.dequeue());
	contador++;
	pila5.push(cola.dequeue());
	contador++;
	pila4.push(cola.dequeue());
	contador++;
	aux.push(cola.dequeue());
	contador++;
	aux.push(cola.dequeue());
	contador++;
	pila2.push(cola.dequeue());
	contador++;
	pila2.push(pila4.pop());
	contador++;
	pila2.push(aux.pop());
	contador++;
	pila5.push(cola.dequeue());
	contador++;
	pila3.push(cola.dequeue());
	contador++;
	pila1.push(cola.dequeue());
	contador++;
	pila1.push(pila4.pop());
	contador++;
	pila2.push(cola.dequeue());
	contador++;
	pila1.push(cola.dequeue());
	contador++;
	pila1.push(pila4.pop());
	contador++;
	aux.push(cola.dequeue());
	contador++;
	pila4.push(cola.dequeue());
	contador++;
	pila4.push(pila5.pop());
	contador++;
	pila4.push(aux.pop());
	contador++;
	pila4.push(aux.pop());
	contador++;
	return contador;
	}

}
