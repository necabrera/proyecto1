package mundo;

public class Nodo<T>
{
	private T item;
	private Nodo<T> next;
	private Nodo<T> prev;
	
	public Nodo()
	{
		next = null;
		prev = null;
	}

	public T getItem()
	{
		return item;
	}

	public Nodo<T> getNext()
	{
		return next;
	}
	
	public Nodo<T> getPrev()
	{
		return prev;
	}

	public void setItem( T pItem)
	{
		item = pItem; 
	}

	public void setNext (Nodo nodo)
	{
		next = nodo;
	}
	
	public void setPrev (Nodo pNodo)
	{
		prev = pNodo;
	}
}
