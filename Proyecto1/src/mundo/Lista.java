package mundo;

public class Lista<T> 
{
	private Nodo<T> primero;
	private Nodo<T> ultimo;
	private int size;

	public Lista()
	{
		primero = null;
		ultimo = null;
		size = 0;
	}

	public Nodo<T> darPrimero() 
	{
		return primero;
	}

	public Nodo<T> darUltimo()
	{
		return ultimo;
	}

	public int getSize()
	{
		return size;
	}

	public void agregar(T objeto)
	{
		Nodo<T> nuevo = new Nodo<T>();
		nuevo.setItem(objeto);
		if(primero == null)
		{
			primero = nuevo;
			ultimo = primero;
			size ++;
		}
		else
		{
			ultimo.setNext(nuevo);
			nuevo.setPrev(ultimo);
			ultimo = nuevo;
			size ++;
		}
	}

	public void agregarEnPos(T obj, int pos)
	{
		Nodo<T> actual = primero;
		Nodo<T> nuevo = new Nodo<>();
		nuevo.setItem(obj);
		Nodo<T> temp = null;
		if(pos == 0)
		{
			nuevo.setNext(actual);
			primero.setPrev(nuevo);
			primero = nuevo;
			size++;
		}
		else
		{
			int i = 0;
			while(i < pos && actual != null)
			{
				temp = actual;
				actual = actual.getNext();
				i++;
			}
			nuevo.setNext(actual);
			temp.setNext(nuevo);
			actual.setPrev(nuevo);
			nuevo.setPrev(temp);
			size ++;
		}
	}

	public Nodo<T> darNodoEnPos(int pos)
	{
		Nodo<T> actual = primero;
		int contador = 0;
		while(actual != null && contador < pos)
		{
			actual = actual.getNext();
			contador++;
		}
		return actual;
	}
	
	
}
