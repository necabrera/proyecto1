package mundo;

public class Colisiones 
{
	private int anio;
	
	private String nivel;
	
	private String zona;
	
	private int numero;
	
	private String autoridad;
	
	public Colisiones(int pAnio, String pNivel, String pZona, int pNumero, String pAutoridad)
	{
		anio = pAnio;
		nivel = pNivel;
		zona = pZona;
		numero = pNumero;
		autoridad = pAutoridad;
	}
	
	public int darAnio()
	{
		return anio;
	}
	
	public String darNivel()
	{
		return nivel;
	}
	
	public String darZona()
	{
		return zona;
	}
	
	public int darNumero()
	{
		return numero;
	}
	
	public String darAutoridad()
	{
		return autoridad;
	}

}
