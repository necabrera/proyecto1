package mundo;

import java.io.FileNotFoundException;
import java.io.FileReader; 
import java.io.IOException;  
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

public class ProcesarJson
{
	private ColisionesBoroughs cbFatal;

	private ColisionesBoroughs cbSerious;

	private ColisionesBoroughs cbSlight;

	private Colisiones cFatal;

	private Colisiones cSerious;

	private Colisiones cSlight;

	private Lista<Colisiones> lista;


	public ProcesarJson()
	{
		lista = new Lista<Colisiones>();
		ColisionesBoroughs cbFatal = null;
		ColisionesBoroughs cbSerious = null;
		ColisionesBoroughs cbSlight = null;
		Colisiones cFatal = null;
		Colisiones cSerious= null;
		Colisiones cSlight = null;
	}

	public Lista<Colisiones> leer() throws MalformedURLException, IOException
	{
		Lista<Colisiones> list = new Lista();
		JsonParser parser = new JsonParser();
		try {
			JsonArray arr= (JsonArray) parser.parse(new FileReader("./data/Ward.json"));

			for (int i = 0; i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);
				String zona= obj.get("Ward name").getAsString();
				String autoridadL = obj.get("Local Authority").getAsString();
				for(int j = 0; j < 5; j++)
				{
					String serious = "Serious 201"+j;
					String slight = "Slight 201"+j;
					String fatal = "Fatal 201"+j;
					String sAnio = "201"+j;
					int anio = Integer.parseInt(sAnio);
					int numFatal = obj.get(fatal).getAsInt();
					int numSlight = obj.get(slight).getAsInt();
					int numSerious = obj.get(serious).getAsInt();

					cFatal = new Colisiones(anio, "Fatal", zona, numFatal, autoridadL);
					list.agregar(cFatal);
					cSerious = new Colisiones(anio, "Serious", zona, numSerious, autoridadL);
					list.agregar(cSerious);
					cSlight = new Colisiones(anio, "Slight", zona, numSlight, autoridadL);
					list.agregar(cSlight);

				}
			}
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}

	public Lista<ColisionesBoroughs> leerBroughs()
	{
		Lista<ColisionesBoroughs> list = new Lista();
		JsonParser parser = new JsonParser();
		try {
			JsonArray arr= (JsonArray) parser.parse(new FileReader("./data/Boroughs.json"));

			for (int i = 0; i < arr.size(); i++)
			{
				JsonObject obj = (JsonObject) arr.get(i);
				String area= obj.get("Area").getAsString();
				for(int j = 1999; j < 2015; j++)
				{
					String serious = "Serious "+j;
					String slight = "Slight "+j;
					String fatal = "Fatal "+j;
					int anio = j;
					int numFatal = obj.get(fatal).getAsInt();
					int numSlight = obj.get(slight).getAsInt();
					int numSerious = obj.get(serious).getAsInt();
					cbFatal = new ColisionesBoroughs(anio, "Fatal", area, numFatal);
					list.agregar(cbFatal);
					cbSerious = new ColisionesBoroughs(anio, "Serious", area, numSerious);
					list.agregar(cbSerious);
					cbSlight = new ColisionesBoroughs(anio, "Slight", area, numSlight);
					list.agregar(cbSlight);
				}
			}
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			e.printStackTrace();
		}
		return list;
	}


}

