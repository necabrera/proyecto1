package mundo;

import java.io.IOException;
import java.net.MalformedURLException;

import org.w3c.dom.ls.LSInput;

public class Central 
{	
	private Lista<ColisionesBoroughs>[] listaSeriousAnio;

	private Lista<Colisiones> listaReporteSlight;

	private Lista<ColisionesBoroughs> listaReporteSerious;

	private Lista<Colisiones>[] listaSlightAnio;

	private Lista<Colisiones> listWard;

	private Lista<ColisionesBoroughs> listBoroughs;

	private ProcesarJson json;

	public Central () throws MalformedURLException, IOException
	{
		json = new ProcesarJson();
		listaSeriousAnio = (Lista[]) new Lista[16];
		listWard = new Lista<Colisiones>();
		listBoroughs = new Lista<ColisionesBoroughs>();
		listWard = json.leer();
		listBoroughs = json.leerBroughs();
		for(int i = 0; i < listaSeriousAnio.length ; i++)
		{
			listaSeriousAnio[i] = new Lista<ColisionesBoroughs>();
		}

		listaSlightAnio = (Lista[]) new Lista[5];
		for(int i = 0; i < listaSlightAnio.length ; i++)
		{
			listaSlightAnio[i] = new Lista<Colisiones>();
		}
		listaReporteSlight = new Lista<Colisiones>();
		listaReporteSerious = new Lista<ColisionesBoroughs>();
	}

	public Lista<Colisiones> generarReporteSlight(Lista<Colisiones> lista) throws MalformedURLException, IOException
	{
		Colisiones primero = lista.darPrimero().getItem();
		if(primero != null)
		{
			if(primero.darNivel().equals("Slight") && primero.darNumero() > 40)
			{

				listaReporteSlight.agregar(primero);
			}
			Nodo<Colisiones> siguiente = lista.darPrimero().getNext();
			while(siguiente.getNext() != null)
			{
				if(siguiente.getItem().darNivel().equals("Slight") && siguiente.getItem().darNumero() > 40)
				{
					listaReporteSlight.agregar(siguiente.getItem());
				}
				siguiente = siguiente.getNext();
				primero = siguiente.getItem();
			}
		}
		return listaReporteSlight;
	}

	public Lista<ColisionesBoroughs> generarReporteSerious(Lista<ColisionesBoroughs> lista) throws MalformedURLException, IOException
	{
		ColisionesBoroughs primero = lista.darPrimero().getItem();
		if(primero != null)
		{
			if(primero.darNivel().equals("Serious") && primero.darNumero() > 100)
			{
				listaReporteSerious.agregar(primero);
			}
			Nodo<ColisionesBoroughs> siguiente = lista.darPrimero().getNext();
			while(siguiente.getNext() != null)
			{
				if(siguiente.getItem().darNivel().equals("Serious") && siguiente.getItem().darNumero() > 100)
				{
					listaReporteSerious.agregar(siguiente.getItem());
				}
				siguiente = siguiente.getNext();
				primero = siguiente.getItem();
			}
		}
		return listaReporteSerious;
	}

	public Lista<Colisiones> quickSortLA(Lista<Colisiones> l)
	{

		sortPorLocalA(l, 0, l.getSize() - 1);
		return l;
	}

	public Lista<Colisiones> sortPorLocalA(Lista<Colisiones> pLista, int min, int max) 
	{

		int i = min;
		int j = max;
		Colisiones pivote = pLista.darNodoEnPos(min).getItem();

		while(i <= j)
		{

			while(pLista.darNodoEnPos(i).getItem().darZona().compareTo(pivote.darZona()) < 0)
			{
				i++;
			}
			while(pivote.darZona().compareTo(pLista.darNodoEnPos(j).getItem().darZona()) < 0)
			{
				j--;
			}
			if(i <= j)
			{
				intercambiar(pLista, i, j);
				i++;
				j++;
			}
		}
		if(min < j)
		{
			sortPorLocalA(pLista, min, j);
		}
		if(i < max)
		{
			sortPorLocalA(pLista, i, max);
		}
		return pLista;
	}

	private void intercambiar(Lista<Colisiones> pLista, int i, int j) 
	{
		Colisiones temporal = pLista.darNodoEnPos(i).getItem();
		pLista.agregarEnPos(pLista.darNodoEnPos(j).getItem(), i);
		pLista.agregarEnPos(temporal, j );
	}

	public void darColisionesSerious() throws MalformedURLException, IOException
	{
		Lista<ColisionesBoroughs> lista = listBoroughs;
		ColisionesBoroughs primero = lista.darPrimero().getItem();
		Nodo<ColisionesBoroughs> nodoP = lista.darPrimero().getNext();
		while(nodoP.getNext() != null)
		{
			primero = nodoP.getItem();
			if(primero.darNivel().equals("Serious") && primero.darAnio() == 1999)
			{
				listaSeriousAnio[0].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2000)
			{
				listaSeriousAnio[1].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2001)
			{
				listaSeriousAnio[2].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2002)
			{
				listaSeriousAnio[3].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2003)
			{
				listaSeriousAnio[4].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2004)
			{
				listaSeriousAnio[5].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2005)
			{
				listaSeriousAnio[6].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2006)
			{
				listaSeriousAnio[7].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2007)
			{
				listaSeriousAnio[8].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2008)
			{
				listaSeriousAnio[9].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2009)
			{
				listaSeriousAnio[10].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2010)
			{
				listaSeriousAnio[11].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2011)
			{
				listaSeriousAnio[12].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2012)
			{
				listaSeriousAnio[13].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2013)
			{
				listaSeriousAnio[14].agregar(primero);
			}
			else if(primero.darNivel().equals("Serious") && primero.darAnio() == 2014)
			{
				listaSeriousAnio[15].agregar(primero);

			}
			nodoP = nodoP.getNext();
		}
	}

	public void darColisionesSlight() throws MalformedURLException, IOException
	{
		Lista<Colisiones> lista = listWard;
		Colisiones primero = lista.darPrimero().getItem();
		Nodo<Colisiones> nodoP = lista.darPrimero();
		while(nodoP.getNext() != null)
		{
			if(primero.darNivel().equals("Slight") && primero.darAnio() == 2010)
			{
				listaSlightAnio[0].agregar(primero);
			}
			else if(primero.darNivel().equals("Slight") && primero.darAnio() == 2011)
			{
				listaSlightAnio[1].agregar(primero);

			}
			else if(primero.darNivel().equals("Slight") && primero.darAnio() == 2012)
			{
				listaSlightAnio[2].agregar(primero);

			}
			else if(primero.darNivel().equals("Slight") && primero.darAnio() == 2013)
			{
				listaSlightAnio[3].agregar(primero);

			}
			else if(primero.darNivel().equals("Slight") && primero.darAnio() == 2014)
			{
				listaSlightAnio[4].agregar(primero);
			}
			nodoP = nodoP.getNext();
			primero = nodoP.getItem();
		}
	}

	public String darWardMasSlight(int anioI, int anioF)
	{
		int num = 0;
		String elWard ="";
		Lista<String> list = new Lista<String>();
		int cant = 0;
		Nodo<Colisiones> nodoP = listWard.darPrimero();
		String wardMas = nodoP.getItem().darZona();
		while(nodoP != null)
		{
			if(wardMas.equals(nodoP.getItem().darZona()))
			{
				if(nodoP.getItem().darAnio() >= anioI && nodoP.getItem().darAnio() <= anioF && nodoP.getItem().darNivel().equals("Slight") )
				{
					num += nodoP.getItem().darNumero();
					
				}
			}
			else if(!wardMas.equals(elWard))
			{
				list.agregar(new String(num +";" + nodoP.getPrev().getItem().darZona()));
				num = 0;
				wardMas = nodoP.getItem().darZona();
			}
			nodoP = nodoP.getNext();
		}
		Nodo <String> nodoS = list.darPrimero();
		String mayorWard = "";
		while(nodoS != null)
		{
			String[] sp = nodoS.getItem().split(";");
			int numWard = Integer.parseInt(sp[0]);
			if(numWard > cant)
			{
				cant = numWard;
				mayorWard = sp[1];
			}
			nodoS = nodoS.getNext();
		}
		return "El ward con mayor cantidad de colisiones Slight es: " + mayorWard + " con un total de: " + cant; 
	}

	public Lista<String> darTotalSlight(int anioI, int anioF)
	{
		int num = 0;
		String ward = "";
		Lista<String> lista = new Lista<String>();
		for(int i = anioI - 2010; i < (anioF + 1) - 2010; i++)
		{
			Nodo<Colisiones> nodoP = listaSlightAnio[i].darPrimero();
			int total = -1;
			while(nodoP != null)
			{
				total = nodoP.getItem().darNumero();
				if(ward != nodoP.getItem().darZona())
				{
					ward = nodoP.getItem().darZona();
					if(num != total)
					{
						num += total;
					}
					System.out.println(num + ward);
					lista.agregar(new String(num +";"+ ward));
				}
				nodoP = nodoP.getNext();
			}
		}
		return lista;
	}

	public Lista<Colisiones> darHistorialColisionesWard(String pWard )
	{
		Lista<Colisiones> listaH = new Lista<Colisiones>();
		for(int i = 0; i < listaSlightAnio.length; i++ )
		{
			Colisiones primero = listaSlightAnio[i].darPrimero().getItem();
			Nodo<Colisiones> nodoP = listaSlightAnio[i].darPrimero();
			while(nodoP != null && primero != null)
			{
				primero = nodoP.getItem();
				if(primero.darZona().equals(pWard))
				{
					listaH.agregar(nodoP.getItem());
				}
				nodoP = nodoP.getNext();
			}
		}
		return listaH;
	}

	public String darAreaMasSerious(int anioI, int anioF)
	{
		int num = 0;
		String elWard ="";
		Lista<String> list = new Lista<String>();
		int cant = 0;
		Nodo<ColisionesBoroughs> nodoP = listBoroughs.darPrimero();
		String wardMas = nodoP.getItem().darArea();
		while(nodoP != null)
		{
			if(wardMas.equals(nodoP.getItem().darArea()))
			{
				if(nodoP.getItem().darAnio() >= anioI && nodoP.getItem().darAnio() <= anioF && nodoP.getItem().darNivel().equals("Serious") )
				{
					num += nodoP.getItem().darNumero();
				}
			}
			else if(!wardMas.equals(elWard))
			{
				list.agregar(new String(num +";" + nodoP.getPrev().getItem().darArea()));
				num = 0;
				wardMas = nodoP.getItem().darArea();
			}
			nodoP = nodoP.getNext();
		}
		Nodo <String> nodoS = list.darPrimero();
		String mayorWard = "";
		while(nodoS != null)
		{
			String[] sp = nodoS.getItem().split(";");
			int numWard = Integer.parseInt(sp[0]);
			if(numWard > cant)
			{
				cant = numWard;
				mayorWard = sp[1];
			}
			nodoS = nodoS.getNext();
		}
		return "El area con mayor cantidad de colisiones Serious es: " + mayorWard + " con un total de: " + cant; 
	}

	public Lista<ColisionesBoroughs> darHistorialColisionesSerious(String pArea )
	{
		Lista<ColisionesBoroughs> listaHS = new Lista<ColisionesBoroughs>();
		for(int i = 0; i < listaSeriousAnio.length; i++ )
		{
			ColisionesBoroughs primero = listaSeriousAnio[i].darPrimero().getItem();
			Nodo<ColisionesBoroughs> nodoP = listaSeriousAnio[i].darPrimero();
			while(nodoP != null && primero != null)
			{
				primero = nodoP.getItem();
				if(primero.darArea().equals(pArea))
				{
					listaHS.agregar(nodoP.getItem());
				}
				nodoP = nodoP.getNext();
			}
		}
		return listaHS;
	}

	public double darPromedioColisionesSlight(String pWard)
	{
		int total = 0;
		int num = 0;
		for(int i = 0; i < listaSlightAnio.length; i++)
		{
			Nodo<Colisiones> nodoP = listaSlightAnio[i].darPrimero();
			while(nodoP != null)
			{
				if(nodoP.getItem().darZona().equals(pWard))
				{
					total += nodoP.getItem().darNumero();
					num ++;
				}
				nodoP = nodoP.getNext();
			}
		}
		return total/num;
	}

	public Lista<Colisiones> darReporteUltimoAnioSlight(String pWard)
	{
		int pos = listaSlightAnio.length;
		Lista<Colisiones> lista = new Lista<Colisiones>();
		Nodo<Colisiones> nodoP = listaSlightAnio[pos - 1].darPrimero();
		while(nodoP != null)
		{
			if(nodoP.getItem().darZona().equals(pWard))
			{
				lista.agregar(nodoP.getItem());
			}
			nodoP = nodoP.getNext();
		}
		return lista;
	}

	public double darPromedioColisionesSerious(String pArea)
	{
		int total = 0;
		int num = 0;
		for(int i = 0; i < listaSeriousAnio.length; i++)
		{
			Nodo<ColisionesBoroughs> nodoP = listaSeriousAnio[i].darPrimero();
			while(nodoP != null)
			{
				if(nodoP.getItem().darArea().equals(pArea))
				{
					total += nodoP.getItem().darNumero();
					num ++;
				}
				nodoP = nodoP.getNext();
			}
		}
		return total/num;
	}

	public Lista<ColisionesBoroughs> darReporteUltimoAnioSerious(String pArea)
	{
		int pos = listaSeriousAnio.length;
		Lista<ColisionesBoroughs> lista = new Lista<ColisionesBoroughs>();
		Nodo<ColisionesBoroughs> nodoP = listaSeriousAnio[pos - 1].darPrimero();
		while(nodoP != null)
		{
			if(nodoP.getItem().darArea().equals(pArea))
			{
				lista.agregar(nodoP.getItem());
			}
			nodoP = nodoP.getNext();
		}
		return lista;
	}

	public int darTotalSlight()
	{
		int total = 0;
		for(int i = 0; i < listaSlightAnio.length; i++)
		{
			Nodo<Colisiones> nodoP = listaSlightAnio[i].darPrimero();
			while(nodoP != null)
			{
				total += nodoP.getItem().darNumero();
				nodoP = nodoP.getNext();
			}
		}
		return total;
	}
}
