package mundo;

public class ColisionesBoroughs 
{
private int anio;
	
	private String nivel;
	
	private String area;
	
	private int numero;
	
	public ColisionesBoroughs(int pAnio, String pNivel, String pArea, int pNumero)
	{
		anio = pAnio;
		nivel = pNivel;
		area = pArea;
		numero = pNumero;
	}
	
	public int darAnio()
	{
		return anio;
	}
	
	public String darNivel()
	{
		return nivel;
	}
	
	public String darArea()
	{
		return area;
	}
	
	public int darNumero()
	{
		return numero;
	}
}
