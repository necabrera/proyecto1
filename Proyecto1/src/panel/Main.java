package panel;
// clase basada en main del taller 3
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException; 
import java.io.InputStreamReader;
import java.net.MalformedURLException;

import mundo.Central;
import mundo.Colisiones;
import mundo.ColisionesBoroughs;
import mundo.Lista;
import mundo.Nodo;
import mundo.ProcesarJson;


public class Main 
{
	private ProcesarJson json;
	private Central central;
	private BufferedReader br;
	private Lista<Colisiones> listaSlight;
	private Lista<ColisionesBoroughs> listaSerious;

	public Main() throws Exception
	{
		br = new BufferedReader(new InputStreamReader(System.in));
		json = new ProcesarJson();
		central = new Central();
		listaSlight = new Lista<Colisiones>();
		listaSerious = json.leerBroughs();
		sort();
//		leerEntrada();
//		leerSalida();
//		leerSolicitudes();
		//		menuPrincipal();
	}
	
	public void sort() throws MalformedURLException, IOException
	{
		Lista<Colisiones> col = json.leer();
		Lista<Colisiones> l = central.quickSortLA(col);
		Nodo<Colisiones> primero = l.darPrimero();
		Colisiones c = l.darPrimero().getItem();
	}

//	public void leerEntrada()
//	{
//		try
//		{
//			System.out.println("------------------------------- lectura de Ingreso_al_parqueadero.csv ------------------------------" );
//			BufferedReader bfr = new BufferedReader(new FileReader("./data/Ingreso_al_parqueadero.csv"));
//			while(bfr.readLine() != null)
//			{
//				System.out.println(bfr.readLine());
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	public void leerSalida()
//	{
//		try
//		{
//			System.out.println("------------------------------- lectura de Salida_del_parqueadero.csv ------------------------------" );
//			BufferedReader bfr = new BufferedReader(new FileReader("./data/Salida_del_parqueadero.csv"));
//			while(bfr.readLine() != null)
//			{
//				System.out.println(bfr.readLine());
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//	
//	public void leerSolicitudes()
//	{
//		try
//		{
//			System.out.println("-------------------------------lectura de Solicitudes_Alcaldes.csv ------------------------------" );
//			BufferedReader bfr = new BufferedReader(new FileReader("./data/Solicitudes_Alcaldes.csv"));
//			while(bfr.readLine() != null)
//			{
//				System.out.println(bfr.readLine());
//			}
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
//	//	public void menuPrincipal() throws Exception
//	//	{
//	//		String mensaje = "Men� principal: \n"
//	//				+ "1. Ver reportes de colisiones Slight segun archivo \n"
//	//				+ "2. Ver reportes de colisiones Serious segun archivo \n"
//	//				+ "3. Ver ward con mas colisiones  Slight en un rango de a�os \n"
//	//				+ "4. Ver Area con mas colisiones  Serious en un rango de a�os \n"
//	//				+ "5. Ver reporte colisiones Slight \n"
//	//				+ "6. Ver reporte colisiones Serious \n"
//	//				+ "7. Salir \n\n"
//	//				+ "Opcion: ";
//	//
//	//		boolean terminar = false;
//	//		while ( !terminar )
//	//		{
//	//			System.out.print(mensaje);
//	//			int op1 = Integer.parseInt(br.readLine());
//	//			while (op1>7 || op1<1)
//	//			{
//	//				System.out.println("\nERROR: Ingrese una opci�n valida\n");
//	//				System.out.print(mensaje);
//	//				op1 = Integer.parseInt(br.readLine());
//	//			}
//	//
//	//			switch(op1)
//	//			{
//	//			case 1: verReportesSlightArchivo(); break;
//	//			case 2: parquearCarro(); break;
//	//			case 3: salidaCliente(); break;
//	//			case 4: verEstadoParquederos(); break;
//	//			case 5: verCarrosEnCola(); break;
//	//			case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
//	//			}
//	//		}
//	//
//	//	}
//
//	public void verReportesSlightArchivo()
//	{
//		try 
//		{
//			Lista<Colisiones> laLista = central.generarReporteSlight(listaSlight );
//			BufferedReader bfr = new BufferedReader(new FileReader("./data/Solicitudes_Alcaldes.csv"));
//			bfr.readLine();
//			String laZona = "";
//			for(int i = 0; i < 17; i++)
//			{
//				String st = bfr.readLine();
//				String[] split = st.split(";");
//				String zona = split[0];
//				Nodo<Colisiones> nodoP = laLista.darPrimero();
//				Colisiones primero = laLista.darPrimero().getItem();
//				while(nodoP != null && primero != null)
//				{
//					primero = nodoP.getItem();
//					if(primero.darAutoridad().equals(zona))
//					{
//						if(!primero.darAutoridad().equals(laZona))
//						{
//							System.out.println("zona: " + zona );
//							laZona = primero.darAutoridad();
//						}
//						System.out.println(primero.darAnio() +": " + primero.darNumero());
//					}
//					nodoP = nodoP.getNext();
//				}
//			}
//		}
//		catch (FileNotFoundException e) 
//		{
//			e.printStackTrace();
//		}
//		catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//	}
//


	public static void main(String[] args) 
	{
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
