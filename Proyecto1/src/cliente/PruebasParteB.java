package cliente;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import mundo.Central;
import mundo.Colisiones;
import mundo.ColisionesBoroughs;
import mundo.Lista;
import mundo.Nodo;
import mundo.ProcesarJson;

public class PruebasParteB {
	
	BufferedWriter escritor;
	Scanner lector;
	private Central central;
	private ProcesarJson json;
	private Lista<ColisionesBoroughs> listaSerious;
	//TODO: Declarar objetos de la parte B
	
	public PruebasParteB(BufferedWriter escritor, Scanner lector) throws MalformedURLException, IOException 
	{
		this.escritor = escritor;
		this.lector = lector;
		central = new Central();
		json = new ProcesarJson();
		listaSerious = new Lista<ColisionesBoroughs>();
	}
	
	public void pruebas() throws MalformedURLException, IOException {
		int opcion = -1;
		listaSerious = json.leerBroughs();
		central.darColisionesSerious();
		//TODO: Inicializar objetos de la parte B
		
		
		long tiempoDeCarga = System.nanoTime();
		//TODO: Cargar informacion de la parte B
		
		
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto B---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Generar reporte de colisiones Serious que\n     estan por encima del maximo permitido\n");
				escritor.write("2: Area con el indice de colisiones Serios mas\n     alto en un periodo de tiempo\n");
				escritor.write("3: Historial de colisiones Serios para un area\n");
				escritor.write("4: Promedio de colisiones Serious para un area\n");
				escritor.write("5: Valor mas reciente de colisiones Serious de\n     un area\n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: reporte1(); break;
				case 2: reporte2(); break;
				case 3: reporte3(); break;
				case 4: reporte4(); break;
				case 5: reporte5(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void reporte1() throws IOException{
		long tiempo = System.nanoTime();
		//TODO: Generar reporte de colisiones Serios que estan por encima del maximo permitido
		Lista<ColisionesBoroughs> lista = central.generarReporteSerious(listaSerious);
		Nodo<ColisionesBoroughs> nodoP = lista.darPrimero();
		while(nodoP != null)
		{
			System.out.println("Area: " + nodoP.getItem().darArea() + " total: " + nodoP.getItem().darNumero());
			nodoP = nodoP.getNext();
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte2() throws IOException{
		escritor.write("Ingrese un periodo de tiempo con el formato AAAA-AAAA\n");
		escritor.flush();
		lector.nextLine();
		String[] periodo = lector.nextLine().trim().split("-");
		if (periodo.length < 2) {
			throw new NumberFormatException();
		}
		int inicio = Integer.parseInt(periodo[0].trim());
		int fin = Integer.parseInt(periodo[1].trim());
		long tiempo = System.nanoTime();
		//TODO: Generar reporte del area con el indice de colisiones Serious mas alto en el rango de fechas 'inicio'-'fin'
		System.out.println(central.darAreaMasSerious(inicio, fin));
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Inicio: " + inicio + "\n");
		escritor.write("fin: " + fin + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte3() throws IOException{
		escritor.write("Ingrese un area\n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar reporte historial de colisiones Serious para el 'area'
		Lista<ColisionesBoroughs> lista = central.darHistorialColisionesSerious(area);
		Nodo<ColisionesBoroughs> nodoP = lista.darPrimero();
		while(nodoP != null)
		{
			System.out.println("A�o: " + nodoP.getItem().darAnio() + " total: " + nodoP.getItem().darNumero());
			nodoP = nodoP.getNext();
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + area + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte4() throws IOException{
		escritor.write("Ingrese un area\n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar el promedio de colisiones Serious para el 'area'
		System.out.println(central.darPromedioColisionesSerious(area));
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + area + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte5() throws IOException{
		escritor.write("Ingrese un area\n");
		escritor.flush();
		lector.nextLine();
		String area = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar el valor mas reciente (ultimo ano) de colisiones Serious para el 'area'
		Lista<ColisionesBoroughs> lista = central.darReporteUltimoAnioSerious(area);
		Nodo<ColisionesBoroughs> nodoP = lista.darPrimero();
		while(nodoP != null)
		{
			System.out.println(" total: " + nodoP.getItem().darNumero());
			nodoP = nodoP.getNext();
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Area: " + area + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
}