package cliente;

import java.io.BufferedWriter; 
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import mundo.Central;
import mundo.Colisiones;
import mundo.ColisionesBoroughs;
import mundo.Lista;
import mundo.Nodo;
import mundo.ProcesarJson;


public class PruebasParteA {
	
	BufferedWriter escritor;
	Scanner lector;
	private Central central;
	private ProcesarJson json;
	private Lista<Colisiones> listaSlight;
	
	//TODO: Declarar objetos de la parte A
	
	public PruebasParteA(BufferedWriter escritor, Scanner lector) throws MalformedURLException, IOException {
		this.escritor = escritor;
		this.lector = lector;
		central = new Central();
		json = new ProcesarJson();
		listaSlight = new Lista<Colisiones>();
	}
	
	public void pruebas() throws MalformedURLException, IOException 
	{
		int opcion = -1;
		listaSlight = json.leer();
		central.darColisionesSlight();
		
		//TODO: Inicializar objetos de la parte A
		
		
		long tiempoDeCarga = System.nanoTime();
		//TODO: Cargar informacion de la parte A
		
		
		tiempoDeCarga = System.nanoTime() - tiempoDeCarga;
		
		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto A---------------\n");
				escritor.write("Informacion cargada en: " + tiempoDeCarga + " nanosegundos\n");
				escritor.write("Reportes:\n");
				escritor.write("1: Generar reporte de autoridades locales con mas\n     de 40 colisiones Slight al ano\n");
				escritor.write("2: Barrios en que mas generaron alarmas Slight\n     en un periodo de tiempo\n");
				escritor.write("3: Historial de colisiones Slight para un barrio\n");
				escritor.write("4: Promedio de colisiones Slight para un barrio\n");
				escritor.write("5: Valor mas reciente de colisiones Slight de un\n     barrio\n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();
				
				switch(opcion) {
				case 1: reporte1(); break;
				case 2: reporte2(); break;
				case 3: reporte3(); break;
				case 4: reporte4(); break;
				case 5: reporte5(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}
	
	private void reporte1() throws IOException{
		long tiempo = System.nanoTime();
		
		//TODO: Generar reporte de las autoridades locales con mas de 40 colisiones Slight al ano
		Lista<Colisiones> lista = central.generarReporteSlight(listaSlight);
		Nodo<Colisiones> nodoP = lista.darPrimero();
		while(nodoP != null)
		{
			System.out.println("Autoridad: " + nodoP.getItem().darAutoridad() + " total: " + nodoP.getItem().darNumero());
			nodoP = nodoP.getNext();
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte2() throws IOException{
		escritor.write("Ingrese un periodo de tiempo con el formato AAAA-AAAA\n");
		escritor.flush();
		lector.nextLine();
		String[] periodo = lector.nextLine().trim().split("-");
		if (periodo.length < 2) {
			throw new NumberFormatException();
		}
		int inicio = Integer.parseInt(periodo[0].trim());
		int fin = Integer.parseInt(periodo[1].trim());
		long tiempo = System.nanoTime();
		//TODO: Generar reporte del barrio que genero mas alarmas Slight en el rango de fechas 'inicio'-'fin'
		System.out.println(central.darWardMasSlight(inicio, fin));
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Inicio: " + inicio + "\n");
		escritor.write("fin: " + fin + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte3() throws IOException{
		escritor.write("Ingrese un barrio\n");
		escritor.flush();
		lector.nextLine();
		String barrio = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar reporte historial de colisiones Slight para el 'barrio'
		Lista<Colisiones> lista = central.darHistorialColisionesWard(barrio);
		Nodo<Colisiones> nodoP = lista.darPrimero();
		while(nodoP != null)
		{
			System.out.println("Autoridad local: " + nodoP.getItem().darAutoridad() +  " \n A�o: " + nodoP.getItem().darAnio() + " \n total: " + nodoP.getItem().darNumero() );
			nodoP = nodoP.getNext();
		}
		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + barrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte4() throws IOException{
		escritor.write("Ingrese un barrio\n");
		escritor.flush();
		lector.nextLine();
		String barrio = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar el promedio de colisiones Slight para el 'barrio'
		System.out.println(central.darPromedioColisionesSlight(barrio));
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + barrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
	
	private void reporte5() throws IOException{
		escritor.write("Ingrese un barrio\n");
		escritor.flush();
		lector.nextLine();
		String barrio = lector.nextLine();
		long tiempo = System.nanoTime();
		//TODO: Generar el valor mas reciente (ultimo ano) de colisiones Slight para el 'barrio'
		Lista<Colisiones> lista = central.darReporteUltimoAnioSlight(barrio);
		Nodo<Colisiones> nodoP = lista.darPrimero();
		while(nodoP != null)
		{
			System.out.println("Autoridad: " + nodoP.getItem().darAutoridad() + " total: " + nodoP.getItem().darNumero());
			nodoP = nodoP.getNext();
		}
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Barrio: " + barrio + "\n");
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}
}